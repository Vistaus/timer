You can set up to 4 timers for your cakes, pizzas, eggs, sport activities or whatever you really need.
Timers can be named and can have an individual sound.

==============

##IMPORTANT!
Please notice that the app depends on the Alarm API which has some limitations. There are a few things you need to know:
1. Setting timers shorter than 1 minute is not recommended. Due to system limitations, the notifications about such timers are not displayed at the right time. Sorry.
2. Sound volume is the same as set in the Clock app for alarms. If 'vibrations' is turned on for alarms in clock app, and 'other vibrations' in system settings, timers will vibrate too.
3. Changing time settings or time zone when a timer is running will confuse the timer.
4. Timers are registered as alarms within the clock app. Once the timer has elapsed it is removed from clock app.

Feel free to report other problems.

==============

Enjoy your time(r)!

Many thanks to Michal Predotka for his great app!
Development is based on his code from: [https://launchpad.net/timer](https://launchpad.net/timer)
