import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: root_about

    header: PageHeader {
        id: main_header

        SwipeToAction {
          apl_page: root_about;
          swipeDistance: swipe_Distance;
          isOneCol: true;
        }

        title: i18n.tr("About")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color}
        leadingActionBar.actions: [
            Action {
                visible: isWide
                text: "close"
                iconName: "close"
                onTriggered: {
                    isOneColumnLayout = true
                    apl_main.removePages(root_about)
                }
            },
            Action {
                id: collapse_favs
                visible: !isWide
                text: "back"
                iconSource: Qt.resolvedUrl(backIcon)
                onTriggered: {
                    isOneColumnLayout = true
                    apl_main.removePages(root_about)
                }
            }
        ]
    }

    Sections {
        id: header_sections
        StyleHints {selectedSectionColor: top_text_color; }
        anchors {
            top: main_header.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        model: [i18n.tr("General"), i18n.tr("Credits"), i18n.tr("Important")]
    }

    Flickable {
        id: page_flickable

        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            top: header_sections.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight:  main_column.height + units.gu(2)
        clip: true

        Column {
            id: main_column

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                leftMargin: units.gu(1)
                rightMargin: units.gu(1)
            }

            Item {
                id: icon

                visible: header_sections.selectedIndex === 0
                width: parent.width
                height: app_icon.height + units.gu(4)

                UbuntuShape {
                    id: app_icon

                    width: Math.min(root_about.width/3, 256)
                    height: width
                    anchors.centerIn: parent

                    source: Image {
                        id: icon_image
                        source: Qt.resolvedUrl("Timer.png")
                    }
                    radius: "small"
                    aspect: UbuntuShape.DropShadow
                }
            }

            Label {
                id: name

                visible: header_sections.selectedIndex === 0
                text: i18n.tr("Timer") + " v%1".arg(Qt.application.version)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: units.gu(4)
                textSize: Label.Large
                horizontalAlignment:  Text.AlignHCenter
            }

            Repeater {
                id: aboutLinks

                model: [
                { name: i18n.tr("Get the sourcecode"), url: "https://gitlab.com/Danfro/timer/" },
                { name: i18n.tr("License") + ": BSD-3-Clause", url: "https://opensource.org/licenses/BSD-3-Clause" },
                { name: i18n.tr("Timer app in OpenStore"), url: "https://open-store.io/app/timerpro.mivoligo" },
                { name: i18n.tr("Report bugs on GitLab"),  url: "https://gitlab.com/Danfro/timer/issues" },
                { name: i18n.tr("Help with translation via GitLab"), url: "https://gitlab.com/Danfro/timer/tree/master/po" },
                { name: i18n.tr("View full changelog"), url: "https://gitlab.com/Danfro/timer/-/blob/master/CHANGELOG.md" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    visible: header_sections.selectedIndex === 0
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        ProgressionSlot {name: "external-link"; color:top_text_color; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            Rectangle {
                // spacer
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            Repeater {
                id: donateLinks

                model: [
                    { name: i18n.tr("Donate to UBports"), subtitle: i18n.tr("UBports is providing Ubuntu Touch."), url: "https://ubports.com/en_EN/donate" },
                    { name: i18n.tr("Donate to Michał Prędotka"), subtitle: i18n.tr("Michał Prędotka developed this great app."), url: "https://www.paypal.me/miv" },
                    { name: i18n.tr("Donate to me"), subtitle: i18n.tr("I am maintaining and improving this app."),url: "https://paypal.me/payDanfro" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    visible: header_sections.selectedIndex === 0
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        subtitle.text: modelData.subtitle
                        ProgressionSlot {name: "external-link"; color:top_text_color; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            Label {
                id: actualdev

                visible: header_sections.selectedIndex === 1
                text: "\n" + i18n.tr("App development since version v1.2")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                divider { visible: false; }
                height: l_maintainer.height + divider.height
                ListItemLayout {
                    id: l_maintainer
                    title.text: i18n.tr("Maintainer") + ": Daniel Frost"
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Danfro')}
            }

            Repeater {
                id: translationsCredits

                model: [
                    { name: i18n.tr("French") + ": Anne Onyme 017, AppsLee"},
                    { name: i18n.tr("Spanish") + ": Krakakanok, advocatux"},
                    { name: i18n.tr("Norwegian") + ": JanC Sherdahl"},
                    { name: i18n.tr("Dutch") + ": Heimen Stoffels"}
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    height: layoutAbout.height
                    visible: header_sections.selectedIndex === 1
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        ProgressionSlot {name: "external-link"; color:top_text_color; }
                    }
                    onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/timer/tree/master/po')
                }
            }

            Rectangle {
                // spacer
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            Label {
                id: historicaldev

                visible: header_sections.selectedIndex === 1
                text: "\n" + i18n.tr("App development up to version v1.1")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            Repeater {
                id: historicalDevCredits

                model: [
                    { name: i18n.tr("Author") + ": Michał Prędotka", url: "http://mivoligo.com" },
                    { name: i18n.tr("Sounds") + ": Tyrel Parker" },
                    { name: i18n.tr("Icon design") + ": Sam Hewitt", url: "https://samuelhewitt.com" },
                    { name: i18n.tr("Testing") + ": Sergi Quiles Pérez"},
                    { name: i18n.tr("Special thanks") + ": nik90"}
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    visible: header_sections.selectedIndex === 1
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        ProgressionSlot {name: "external-link"; color:top_text_color; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            //Warning Label, changes need to go to: /Timer/ui/Warning.qml, README.md and OpenStore description text
            Label {
                id: warning

                visible: header_sections.selectedIndex === 2

                horizontalAlignment: Text.AlignJustify //Text.AlignHCenter
                width: parent.width //- units.gu(2)
                text: "\n"
                    + i18n.tr("Please notice that the app depends on the Alarm API which has some limitations. There are a few things you need to know:")
                    + "\n\n"
                    + "1. " + i18n.tr("Setting timers shorter than 1 minute is not recommended. Due to system limitations, the notifications about such timers are not displayed at the right time. Sorry.")
                    + "\n\n"
                    + "2. " + i18n.tr("Sound volume is the same as set in the Clock app for alarms. If 'vibrations' is turned on for alarms in clock app, and 'other vibrations' in system settings, timers will vibrate too.")
                    + "\n\n"
                    + "3. " + i18n.tr("Changing time settings or time zone when timer is running will confuse the timer.")
                    + "\n\n"
                    + "4. " + i18n.tr("Timers are registered as alarms within the clock app. Once the timer has elapsed it is removed from clock app.")
                    + "\n\n"
                    + "5. " + i18n.tr("You can set up to four timers.")
                    + "\n\n"
                    + i18n.tr("Feel free to report other problems:") + "\n"
                wrapMode: Text.WordWrap
            }
            ListItem {
                visible: header_sections.selectedIndex === 2
                divider.visible: false
                height: bugreport.height

                Label {
                    id: bugreport
                    text: i18n.tr("Report an issue on GitLab")
                    color: theme.palette.normal.activity
                    horizontalAlignment: Text.AlignHLeft

                    MouseArea {
                        anchors.fill: parent
                        onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/timer/issues')
                    }
                }
            }
        }
    }
}
