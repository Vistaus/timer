import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: root_clockthemepage

    property string temp_clock_theme

    header: PageHeader {
        id: main_header

        SwipeToAction {
          apl_page: root_clockthemepage;
          swipeDistance: swipe_Distance;
        }

        title: i18n.tr("Clock theme")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color;}

        leadingActionBar.actions: Action {
            text: "close"
            iconName: "close"
            // iconSource: Qt.resolvedUrl(backIcon)
            onTriggered: {
                apl_main.removePages(root_clockthemepage)
            }
        }

        trailingActionBar.actions: Action {
            text: "confirm"
            iconName: "ok"
            onTriggered: {
                current_clock_theme = temp_clock_theme
                apl_main.removePages(root_clockthemepage)
            }
        }
    }

    ListView {
        id: clock_themes_listview

        width: parent.width
        height: parent.height - main_header.height
        anchors.top: main_header.bottom
        clip: true

        model: ["Colores", "Imitatio", "Standard"]

        delegate:
            ListItem {
            id: theme_item

            height: main_layout.height + divider.height

            ListItemLayout {
                id: main_layout

                title.text: modelData

                Image {
                    source: Qt.resolvedUrl(/*"images/" + */modelData + "/preview.png")
                    width: units.gu(6)
                    height: width
                    SlotsLayout.position: SlotsLayout.Leading
                }
                Icon {
                    name: "tick"
                    color: main_layout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Trailing
                    opacity: modelData == temp_clock_theme ? 1 : 0
                }
            }
            onClicked: {
                temp_clock_theme = modelData
            }
        }
    }
}
