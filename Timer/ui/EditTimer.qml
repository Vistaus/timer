import QtQuick 2.4
import Ubuntu.Components 1.3
import "Storage.js" as Storage

Page {
    id: root_edittimer

    property alias time_setter_date: time_setter.timelength
    property alias timer_name_field: timer_name_field
    property alias timer_name_text: timer_name_field.text
    property alias time_setter: time_setter
    property int timer_lenght: (time_setter.h*60*60 + time_setter.m*60 + time_setter.s)*1000
    property bool isEdit: false // check if it's new fav or edit
    property bool isFromMain: false // check if we got here from main page

    header: PageHeader {
        id: main_header

        title: isEdit ? i18n.tr("Edit timer") : i18n.tr("Add to favourites")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color}
        leadingActionBar.actions: [
            Action {
                text: "close"
                iconName: "close"
                onTriggered: {
                    apl_main.removePages(root_edittimer)
                    if (isFromMain) isOneColumnLayout = true
                }
            }
        ]
        trailingActionBar.actions: [
            Action {
                id: accept_action

                visible: timer_name_field.text.length > 0 && time_text_set.text != "00:00:00"
                iconName: "ok"
                text: i18n.tr("Save")

                onTriggered: {
                    if(!isEdit){ // SAVE NEW FAV
                        var stamp = Date.now().toString()
                        fav_page.fav_listmodel.append({"name_of_fav": timer_name_field.text, "timertime": timer_lenght, "favtimestamp": stamp, "favtimesound": alarmsound})
                        Storage.saveFavs(timer_name_field.text, timer_lenght, stamp, alarmsound)
                        fav_page.fav_listview.positionViewAtEnd()
                        if (isFromMain) {
                            apl_main.removePages(root_edittimer)
                            apl_main.addPageToNextColumn(apl_main.primaryPage, fav_page)
                        }
                        else {
                            apl_main.removePages(root_edittimer)
                        }
                    }
                    else { // EDIT EXISTING FAV
                        var currentstamp = fav_page.fav_listmodel.get(fav_page.fav_listview.currentIndex).favtimestamp
                        fav_page.fav_listmodel.set(fav_page.fav_listview.currentIndex, {"name_of_fav": timer_name_field.text, "timertime": timer_lenght, "favtimesound": alarmsound})
                        Storage.updateFavs(timer_name_field.text, timer_lenght, currentstamp, alarmsound)
                        apl_main.removePages(root_edittimer)
                    }


                }
            }
        ]
    }

    // RESET TIMER TO ZERO
    ClickyIcon {
        id: reset_button

        visible: time_text_set.text != "00:00:00"
        iconcolor: theme.palette.normal.negative
        iconname: "reset"
        x: units.gu(2)
        anchors.verticalCenter: time_text_set.verticalCenter

        onClicked: {
            // reset timer to zero
            time_setter.h = 0
            time_setter.m = 0
            time_setter.s = 0
            time_setter.timelength = 0
        }
    }

    // FOR KEYBOARD NAVIGATION
    Button {
        id: hour_btn

        color: "transparent"
        width: time_text_set.height
        height: width
        anchors {
            left: time_text_set.left
            verticalCenter: time_text_set.verticalCenter
        }

        Keys.onPressed: {
            var key = event.key
            if(keys01.indexOf(key) > -1) {
                if(time_setter.h === 0 || time_setter.h >= 2) {
                    time_setter.h = keys01.indexOf(key)
                }
                else {
                    time_setter.h = 10 * time_setter.h + keys01.indexOf(key)
                }
            }
            else if(keys29.indexOf(key) > -1) {
                time_setter.h = keys29.indexOf(key) + 2
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.h < 11) {
                    time_setter.h += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.h > 0) {
                    time_setter.h -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if(accept_action.visible) {
                    accept_action.trigger()
                }
            }
        }
    }

    Button {
        id: min_btn

        color: "transparent"
        width: time_text_set.height
        height: width
        anchors.centerIn: time_text_set

        Keys.onPressed: {
            var key = event.key
            if(keys05.indexOf(key) > -1) {
                if(time_setter.m === 0 || time_setter.m >= 6) {
                    time_setter.m = keys05.indexOf(key)
                }
                else {
                    time_setter.m = 10 * time_setter.m + keys05.indexOf(key)
                }
            }
            else if(keys69.indexOf(key) > -1) {
                if(time_setter.m === 0 || time_setter.m >= 6) {
                    time_setter.m = keys69.indexOf(key) + 6
                }
                else {
                    time_setter.m = 10 * time_setter.m + (keys69.indexOf(key) + 6)
                }
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.m < 59) {
                    time_setter.m += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.m > 0) {
                    time_setter.m -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if(accept_action.visible) {
                    accept_action.trigger()
                }
            }
        }
    }

    Button {
        id: sec_btn

        color: "transparent"
        width: time_text_set.height
        height: width
        anchors {
            right: time_text_set.right
            verticalCenter: time_text_set.verticalCenter
        }

        Keys.onPressed: {
            var key = event.key
            if(keys05.indexOf(key) > -1) {
                if(time_setter.s === 0 || time_setter.s >= 6) {
                    time_setter.s = keys05.indexOf(key)
                }
                else {
                    time_setter.s = 10 * time_setter.s + keys05.indexOf(key)
                }
            }
            else if(keys69.indexOf(key) > -1) {
                if(time_setter.s === 0 || time_setter.s >= 6) {
                    time_setter.s = keys69.indexOf(key) + 6
                }
                else {
                    time_setter.s = 10 * time_setter.s + (keys69.indexOf(key) + 6)
                }
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.s < 59) {
                    time_setter.s += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.s > 0) {
                    time_setter.s -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if(accept_action.visible) {
                    accept_action.trigger()
                }
            }
        }
    }

    // TIMER TEXT CLOCK
    Text {
        id: time_text_set

        color: top_text_color
        anchors {
            top: main_header.bottom
            horizontalCenter: parent.horizontalCenter
        }
        text: unitsdisplay(time_setter.h) + ":" + unitsdisplay(time_setter.m) + ":" + unitsdisplay(time_setter.s)
        font.pixelSize: parent.width/6
    }

    // EDIT TIME ICON
    ClickyIcon {
        id: edit_time_button

        visible: !time_setter.visible
        iconcolor: top_text_color
        iconname: "edit"
        anchors {
            right: parent.right
            rightMargin: units.gu(2)
            verticalCenter: time_text_set.verticalCenter
        }
        onClicked: {
            // show time setter
            time_setter.visible = true
            time_setter.forceActiveFocus()
        }
    }

    // TIMER NAME
    TextField {
        id: timer_name_field

        width: parent.width - units.gu(4)
        anchors {
            top: time_text_set.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        placeholderText: i18n.tr("Timer name")
        onAccepted: if(accept_action.visible) accept_action.trigger()
        onFocusChanged: if(focus){time_setter.visible = false}
        inputMethodHints: Qt.ImhNoPredictiveText

    }

    // TIMER SOUND
    ListItem {
        id: edit_sound

        anchors{
            top: timer_name_field.bottom
            right: parent.right
            rightMargin: 0
            left: parent.left
        }
        divider.visible: false
        SlotsLayout {
            mainSlot: Label {
                id: sound_name
                text: getFileName(alarmsound)
            }
            Icon {
                color: sound_name.color
                // replace source with name once the icon is available from upstream
                // name: alarmsound === "No sound" ? "disable_stock_music" : "stock_music"
                source: alarmsound === "No sound" ? "disable_stock_music.svg" : "music-app-symbolic.svg"
                SlotsLayout.position: SlotsLayout.Trailing;
                width: units.gu(2)
            }
            ProgressionSlot{}
        }
        onClicked: apl_main.addPageToNextColumn(root_edittimer, Qt.resolvedUrl("DefaultSoundSettingsPage.qml"), {isDefaultSoundEdited: false, temp_sound: alarmsound})
    }

    // TIME SETTER
    TimeSetter {
        id: time_setter

        visible: false
        anchors {
            top: edit_sound.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
            verticalCenter: undefined
        }
        width: root_edittimer.width > root_edittimer.height ? Math.min( parent.width/2, parent.height - units.gu(8), units.gu(60) )
                                                            : Math.min(parent.width - units.gu(4), parent.height - time_text_set.height - timer_name_field.height - units.gu(9), units.gu(60))
    }
    states: [
        State {
            name: "editorlandscape"
            when: root_edittimer.width > root_edittimer.height
            PropertyChanges {
                target: time_setter;
                anchors.top: undefined
                anchors.verticalCenter: root_edittimer.verticalCenter
                anchors.verticalCenterOffset: main_header.height/4
                anchors.horizontalCenterOffset: root_edittimer.width/4
            }
            PropertyChanges {
                target: time_text_set;
                anchors.horizontalCenterOffset: -root_edittimer.width/4
                font.pixelSize: root_edittimer.width/12
            }

            PropertyChanges {
                target: edit_time_button;
                anchors.right: timer_name_field.right
                anchors.rightMargin: 0
            }

            PropertyChanges {
                target: edit_sound
                anchors.right: timer_name_field.right
                anchors.rightMargin: - units.gu(2)
            }

            PropertyChanges {
                target: timer_name_field
                width: root_edittimer.width/2 - units.gu(4)
                anchors.horizontalCenterOffset: -root_edittimer.width/4
                anchors.topMargin: 0
            }
        }
    ]
}
