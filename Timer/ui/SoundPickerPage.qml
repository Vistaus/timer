import QtQuick 2.4
import Ubuntu.Components 1.3
import QtMultimedia 5.6
import Qt.labs.folderlistmodel 2.1


Rectangle {
    id: root_soundpickerpage

    //property alias listview: settings_listview
    property alias audio_play: audio_play
    property string temp_sound

    width: root_soundpickerpage.width > root_soundpickerpage.height && !isHigh ? root_soundpickerpage.width/2 : root_soundpickerpage.width
    //height: stop_play_item.height + timersound_listview.height + systemsound_listview.height

    color: main_back_color

    PageHeader {
        id: main_header

        StyleHints {backgroundColor: main_back_color; foregroundColor: top_text_color}
        leadingActionBar.actions: Action {
            text: "close"
            iconName: "close"
            onTriggered: {
                audio_play.stop()
                sound_ldr.source = ""
            }
        }

        Button {
            id: stop_play_button

            property bool isPlaying: audio_play.playbackState == Audio.PlayingState

            width: Math.min(parent.width - units.gu(4), units.gu(22))
            anchors.centerIn: parent
            text: isPlaying ? i18n.tr("Stop playing") : i18n.tr("Play selected")
            onClicked: isPlaying ? audio_play.stop() : audio_play.play()
        }

        trailingActionBar.actions: Action {
            text: "confirm"
            iconName: "ok"
            onTriggered: {
                audio_play.stop()
                timersound = temp_sound
                if (timersound == "No sound") {
                    audio_play.muted = true
                } else {
                    audio_play.muted = false
                }
                sound_ldr.source = ""
            }
        }
    }

    Audio {
        id: audio_play

        source: Qt.resolvedUrl(getSoundFile(temp_sound))
        loops: Audio.Infinite
        audioRole: Audio.AlarmRole
    }

    Label {
      id: timersoundlabel
      width: parent.width - units.gu(4)
      anchors.horizontalCenter: root_soundpickerpage.horizontalCenter
      anchors.top: main_header.bottom
      anchors.topMargin: units.gu(2)
      text: i18n.tr("Timer sounds")
      font.underline: true
      font.bold: true
    }

     ListView {
          id: timersound_listview

          width: parent.width
          height: root_soundpickerpage.height/2 - main_header.height*2
          anchors.top: timersoundlabel.bottom
          anchors.topMargin: units.gu(2)
          clip: true

          ListModel {
            id: timerSoundModel
            Component.onCompleted: initialise()
            function initialise() {
              timerSoundModel.append({ "modelData": "No sound"})
              timerSoundModel.append({ "modelData": "sounds/Beeps.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Buzz.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Campanula.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Drrr.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Fanfare.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Harp.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Joy.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Military.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Progressive.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Riff.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Tempus.ogg"})
              timerSoundModel.append({ "modelData": "sounds/Woods.ogg"})
            }
          }

          model: timerSoundModel

          delegate:
              ListItem {
              id: timer_sound_item

              height: timer_main_layout.height
              divider.visible: false

              ListItemLayout {
                  id: timer_main_layout
                  title.text: getFileName(modelData)

                  Icon {
                      name: "tick"
                      color: timer_main_layout.title.color
                      width: units.gu(2)
                      height: width
                      SlotsLayout.position: SlotsLayout.Trailing
                      opacity: modelData == temp_sound ? 1 : 0
                  }
              }
              onClicked: {
                  temp_sound = modelData
              }
          }
      }

      Label {
        id: systemsoundlabel
        width: parent.width - units.gu(4)
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: timersound_listview.bottom
        anchors.topMargin: units.gu(2)
        text: i18n.tr("System sounds")
        font.underline: true
        font.bold: true
      }

      ListView {
          id: systemsound_listview

          width: parent.width
        height: root_soundpickerpage.height - (timersound_listview.height + systemsoundlabel.height + main_header.height + units.gu(9) /*apps header height*/)
          anchors.top: systemsoundlabel.bottom
          anchors.topMargin: units.gu(2)
          clip: true

          FolderListModel {
              id: systemSoundModel
              nameFilters: [ "*.ogg", "*.mp3" ]
              folder: "/usr/share/sounds/ubports/ringtones"
          }

          model: systemSoundModel

          delegate:
              ListItem {
              id: system_sound_item

              height: system_main_layout.height
              divider.visible: false

              ListItemLayout {
                  id: system_main_layout
                  title.text: fileBaseName

                  Icon {
                      name: "tick"
                      color: system_main_layout.title.color
                      width: units.gu(2)
                      height: width
                      SlotsLayout.position: SlotsLayout.Trailing
                      opacity: filePath == temp_sound ? 1 : 0
                  }
              }
              onClicked: {
                  temp_sound = filePath
              }
          }
      }
}
