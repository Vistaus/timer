import QtQuick 2.4
import Ubuntu.Components 1.3
import "Storage.js" as Storage

Item {
    id: root_timerelement

    property bool isRunning: isItRunning
    property bool isZero: time.text === "00:00:00"
    property var timeSinceFinish // time duration since timer was finished
    property var timerDuration: durationOfTimer
    property var remainDuration
    property var timerEnd: endTime
    property string timerName: nameOfTimer
    property string timerMessage
    property var timerSound: soundOfTimer
    property string tStamp: timerStamp

    function remainingTime(timerEnd)
    {
        var start = Date.now()
        var end = new Date(timerEnd)
        var remain = end - start

        if (remain >= 0){
            //            return [new_time_setter.timelength = remain,

            //                    timeLenghtToHMS(remain, new_time_setter)
            //                    ]
            return [time.text = timeLenghtToDate(remain/* + 1000*/), // add 1s so 00:00:00 is 0 and not 00:00:00.999
                    remainDuration = remain
                    ]
        }

        else {
            return [timeSinceFinish = timeLenghtToDate(Math.abs(remain)),
                    timer_finished.visible = true
                    ]
        }
    }

    function calcEndTime(timerEnd)
    {
        var endTimeV = Qt.formatTime(new Date(timerEnd), "hh:mm:ss")
        var today = Qt.formatDateTime(new Date(), "d MMM yyyy")
        var endDay = Qt.formatDateTime(new Date(timerEnd), "d MMM yyyy")

        if (today === endDay) {
            return endTimeV
        }
        else {
            return i18n.tr("tomorrow %1").arg(endTimeV)
        }
    }

    function calcTimerDuration(timerEnd)
    {
        var start = Date.now()
        var end = new Date(timerEnd)
        var remain = end - start

        return timerDuration = remain
    }

    function calcNewEndTime()
    {
        var start = Date.now()
        var timer_duration = timerDuration
        var new_end_date = start + timer_duration
        var endTimeV = Qt.formatTime(new Date(new_end_date), "hh:mm:ss")
        var today = Qt.formatDateTime(new Date(), "d MMM yyyy")
        var endDay = Qt.formatDateTime(new Date(new_end_date), "d MMM yyyy")

        if (today === endDay) {
            return endTimeV
        }
        else {
            return i18n.tr("tomorrow %1").arg(endTimeV)
        }
    }

    function stopTimer()
    {

        var timerlengthdigits = timeLenghtToDate(timerDuration)
        calcTimerDuration(timerEnd)
        progress_bar.width = progress_back.width
        timers_model.set(index, {
                             isItRunning: false,
                             durationOfTimer: timerDuration
                         })
        Storage.updateTimers(timerName, timerEnd, timerDuration, timerSound, 0, timers_model.get(index).timerStamp)
        var alarm_message = timerName + " (" + timerlengthdigits + ")"
        var alarm_date = timerEnd
        deleteAlarm(alarm_date, alarm_message)
    }

    function startTimer()
    {

        var timerlengthdigits = timeLenghtToDate(timerDuration)
        var timerend = Date.now() + timerDuration // set new end time
        timerEnd = timerend - (timerend % 1000) // round to 1s
        alarm.reset() // reset Alarm, so it could be changed
        alarm.date = new Date(timerEnd)
        alarm.message = timerName == "" ? i18n.tr("Timer") + " (" + timerlengthdigits + ")"
                                        : timerName + " (" + timerlengthdigits + ")"
        alarm.sound = getSoundFile(timerSound)
        alarm.save()
        timers_model.set(index, {
                             "nameOfTimer": timerName,
                             "endTime": timerEnd,
                             "soundOfTimer": timerSound,
                             "durationOfTimer": timerDuration,
                             "isItRunning": true
                         })

        Storage.updateTimers(timerName, timerEnd, timerDuration, timerSound, 1, timers_model.get(index).timerStamp)
    }

    function removeTimer()
    {
        tStamp = timers_model.get(index).timerStamp
        var timerlengthdigits = timeLenghtToDate(timerDuration)
        var alarm_message = timerName + " (" + timerlengthdigits + ")"
        var alarm_date = timerEnd
        deleteAlarm(alarm_date, alarm_message)
        Storage.deleteTimers(tStamp)
        timers_model.remove(index)
    }

    onTimerDurationChanged: if(!isRunning) {
                                time.text = timeLenghtToDate(timerDuration)
                            }


    Timer {
        id: refreshTimer

        interval: 100
        repeat: true
        running: isRunning

        onTriggered: {
            remainingTime(timerEnd)
        }
    }

    Timer {
        id: check_end_time_timer

        running: isRunning
        interval: 100

        onTriggered: {
            // TRANSLATORS: It's about the time at which a timer will finish,
            // It will read for example "Finish 12:45"
            finish_time.text = i18n.tr("Finish %1").arg(calcEndTime(timerEnd))
        }

    }

    LiveTimer {
        id: checkNewEndTime

        frequency: isRunning ? LiveTimer.Disabled : LiveTimer.Second
        onTrigger: finish_time.text = i18n.tr("Finish %1").arg(calcNewEndTime())
    }

    Item {
        id: timer_side

        visible: !timer_finished.visible
        anchors.fill: parent

        // Timer progress
        Rectangle {
            id: progress_back

            visible: isRunning
            color: progress_back_color
            width: parent.width - units.gu(4)
            height: units.dp(3)
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
            }

            Rectangle {
                id: progress_bar

                color: progress_color
                width: parent.width
                height: parent.height
                Behavior on width { NumberAnimation {duration: 165} }
                Connections {
                    target: time
                    onTextChanged: progress_bar.width = progress_back.width * (remainDuration - 1000) / timerDuration
                }
            }
        }

        // Timer name
        Label {
            id: name

            anchors {
                horizontalCenter: parent.horizontalCenter
                top: progress_back.bottom
                topMargin: units.gu(1)
            }
            text: timerName
        }

        // Timer time
        Label {
            id: time

            color: isRunning ? top_text_color : theme.palette.normal.activity
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: name.bottom
                topMargin: units.dp(2)
            }
            text: if (!isRunning) timeLenghtToDate(timerDuration)
            font.pixelSize: Math.min((parent.height - name.height - finish_time.height - progress_back.height - units.gu(4) - units.dp(6)) / 2, parent.width / 6 )
        }

        // Edit/Preview timer
        Button {
            id: edit_timer_btn

            color: "transparent"
            width: time.width
            height: time.height
            anchors.centerIn: time

            onClicked:  {
                timers_grid.currentIndex = index
                main_page.header = timer_preview_header
                timer_preview_ldr.source = Qt.resolvedUrl("TimerPreview.qml")

                timer_preview_ldr.item.tempendtime = timerEnd
                timer_preview_ldr.item.timername = timerName
                timer_preview_ldr.item.timerduration = timerDuration
                timer_preview_ldr.item.timersound = timerSound
                timer_preview_ldr.item.isRunning = isRunning
                isRunning ? timeLenghtToHMS(remainDuration, timer_preview_ldr.item.time_setter)
                          : timeLenghtToHMS(timerDuration, timer_preview_ldr.item.time_setter)

            }
        }

        // Remove timer
        ClickyIcon {
            id: delete_timer_btn

            visible: !isRunning
            iconcolor: theme.palette.normal.negative
            iconname: "delete"
            anchors {
                right: time.left
                rightMargin: units.gu(1)
                verticalCenter: time.verticalCenter
            }

            onClicked: removeTimer() // delete timer
        }

        // Timer finish time
        Label {
            id: finish_time

            visible: !isZero
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: time.bottom
                topMargin: units.dp(2)
            }
        }

        // Stop timer button
        Button {
            id: stop_timer_btn

            visible: !isZero
            color: isRunning ? theme.palette.normal.negative : theme.palette.normal.activity
            width: Math.min(parent.width - units.gu(4), units.gu(22) )
            height: units.gu(5)
            anchors{
                top: finish_time.bottom
                topMargin: units.gu(1)
                horizontalCenter: parent.horizontalCenter
            }

            Icon {
                id: start_button_icon
                color: theme.palette.normal.activityText
                name: isRunning ? "media-playback-stop" : "media-playback-start"
                width: parent.height/2
                anchors.centerIn: parent
            }

            onClicked: isRunning ? stopTimer() : startTimer()
        }

    }

    // Timer finished message
    Rectangle {
        id: timer_finished

        visible: false
        anchors.fill: parent
        anchors.margins: units.gu(2)

        Label {
            id: since_finish_label

            width: parent.width - units.gu(4)
            anchors {
                top: parent.top
                topMargin: units.gu(1)
            }

            text: {
                var timerlengthdigits = timeLenghtToDate(timerDuration)
                timerName + " (" + timerlengthdigits + ")" + i18n.tr(" finished %1 ago").arg(timeSinceFinish)
            }

            horizontalAlignment:  Text.AlignHCenter
            wrapMode: Text.WordWrap
        }

        Button {
            id: okay_button

            width: Math.min(parent.width - units.gu(4), units.gu(22) )
            height: units.gu(5)
            text: i18n.tr("Okay")
            color: theme.palette.normal.activity
            anchors {
                bottom: parent.bottom
                bottomMargin: units.gu(2)
                horizontalCenter: parent.horizontalCenter
            }

            onClicked: {
                removeTimer() // delete timer
            }
        }
    }

}
