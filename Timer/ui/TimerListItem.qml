import QtQuick 2.4
import Ubuntu.Components 1.3
import "Storage.js" as Storage

ListItem {
    id: root_timerlistitem

    /*
    defines the list items for timer favourites
    called as delegate in FavPage.qml
    */

    property var stamp: favtimestamp
    property var timersound: favtimesound
    property bool isItemClickable: isUnlocked && (main_page.timers_model.count < 4)

    divider.visible: false
    height: main_layout.height

    // override coloring of leading action: red background, white icon
    // StyleHints {leadingPanelColor: theme.palette.normal.negative; leadingForegroundColor: theme.palette.normal.raised}

    ListItemLayout {
        id: main_layout
        // TIMER NAME TEXT
        title.color: isItemClickable ? theme.palette.normal.foregroundText : theme.palette.disabled.foregroundText
        subtitle.color: isItemClickable ? theme.palette.normal.baseText : theme.palette.disabled.baseText
        title.text: name_of_fav
        subtitle.text: getFileName(favtimesound)

        // TIMER TIME TEXT
        Label {
            color: isItemClickable ? theme.palette.normal.foregroundText : theme.palette.disabled.foregroundText
            text: timeLenghtToDate(timertime)
            textSize: Label.Large
        }
    }

    leadingActions: ListItemActions {
        actions: [
            Action {
                iconName: "delete"
                onTriggered: {
                    fav_listview.currentIndex = index
                    var currentstamp = fav_listmodel.get(fav_listview.currentIndex).favtimestamp
                    fav_listmodel.remove(fav_listview.currentIndex)
                    Storage.deleteFavs(currentstamp)
                }
            }
        ]
    }

    trailingActions: ListItemActions {
        actions: [
            Action {
                iconName: "edit"
                onTriggered: {
                    fav_listview.currentIndex = index
                    var timersound = fav_listmodel.get(fav_listview.currentIndex).favtimesound
                    apl_main.addPageToCurrentColumn(root_favpage, edit_timer_page, {isEdit: true, isFromMain: false, timer_name_text: name_of_fav, time_setter_date: timertime})
                    timersound ? alarmsound = timersound : alarmsound = defaultsound
                    timeLenghtToHMS(timertime, edit_timer_page.time_setter)
                }
            }
        ]
    }

    onPressAndHold: fav_listview.ViewItems.selectMode = true
    onClicked: {
        if (/*!isTimerRunning*/isItemClickable  && !fav_listview.ViewItems.selectMode) {
            fav_listview.currentIndex = index
            var timestamp = Date.now().toString()
            var timername = fav_listmodel.get(fav_listview.currentIndex).name_of_fav
            var timersound = fav_listmodel.get(fav_listview.currentIndex).favtimesound
            var timerduration = fav_listmodel.get(fav_listview.currentIndex).timertime
//            timeLenghtToHMS(timertime, main_page.main_time_setter.time_setter)
//            alarm_title = name_of_fav
            timersound ? alarmsound = timersound : alarmsound = defaultsound

            main_page.timers_model.append({"nameOfTimer": timername,
                                    "endTime": 0,
                                    "soundOfTimer": timersound,
                                    "timerStamp": timestamp,
                                    "durationOfTimer": timerduration,
                                    "isItRunning": false
                                })
            Storage.saveTimers(name_of_fav, 0, timertime, timersound, 0, timestamp)

            if(!isWide) {
                isOneColumnLayout = true
                apl_main.removePages(root_favpage)
            }
        }
        else if (fav_listview.ViewItems.selectMode){
            selected = !selected
        }
    }
}
