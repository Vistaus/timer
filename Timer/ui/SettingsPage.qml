import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: root_settingpage

    header: PageHeader {
        id: main_header

        SwipeToAction {
          apl_page: root_settingpage;
          swipeDistance: swipe_Distance;
          isOneCol: true;
        }

        title: i18n.tr("Settings")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color;}
                leadingActionBar.actions: [
                    Action {
                        visible: isWide
                        text: "close"
                        iconName: "close"
                        onTriggered: {
                            isOneColumnLayout = true
                            apl_main.removePages(root_settingpage)
                        }
                    },
                    Action {
                        visible: !isWide
                        text: "close"
                        iconSource: Qt.resolvedUrl(backIcon)
                        onTriggered: {
                            isOneColumnLayout = true
                            apl_main.removePages(root_settingpage)
                        }
                    }
                ]
    }

    Flickable {
      id: page_flickable

      anchors {
        top: main_header.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
      }

      flickableDirection: Flickable.VerticalFlick
      contentHeight: settings_column.height + units.gu(4)
      clip: true

      Column {
          id: settings_column

          anchors {
              top: parent.top
              left: parent.left
              right: parent.right
              //do not set bottom, otherwise we can't flick up to the bottom of the page
          }

          ListItem {
              id: display_on_setting_item
              height: l_displ.height
              divider.visible: false

              ListItemLayout {
                  id: l_displ
                  title.text: i18n.tr("Keep display on")

                  CheckBox {
                      id: display_checkbox
                      checked: isDisplayOn
                      onCheckedChanged: isDisplayOn = checked
                      SlotsLayout.position: SlotsLayout.Leading
                  }
              }
              onClicked: display_checkbox.checked = !display_checkbox.checked
          }

          ListItem {
              id: sound_setting_item
              height: l_sound.height
              divider.visible: false

              ListItemLayout {
                  id: l_sound
                  title.text: i18n.tr("Default sound")

                  Label { text: getFileName(defaultsound) }
                  ProgressionSlot{}
              }
              onClicked: {
                  apl_main.addPageToCurrentColumn(root_settingpage, Qt.resolvedUrl("DefaultSoundSettingsPage.qml"), {isDefaultSoundEdited: true, temp_sound: defaultsound})
              }
          }

          ListItem {
              id: clock_theme_setting_item
              height: clockthemeLayout.height
              divider.visible: false

              ListItemLayout {
                  id: clockthemeLayout
                  title.text: i18n.tr("Clock theme")

                  Label { text: current_clock_theme }
                  ProgressionSlot{}
              }
              onClicked: apl_main.addPageToCurrentColumn(root_settingpage, Qt.resolvedUrl("ClockThemePage.qml"), {temp_clock_theme: current_clock_theme})
          }

          ListItem {
              id: app_theme_setting_item
              height: appthemeLayout.height
              divider.visible: false

              ListItemLayout {
                  id: appthemeLayout
                  title.text: i18n.tr("App theme")

                  Label { text: current_app_Theme }
                  ProgressionSlot{}
              }
              onClicked: apl_main.addPageToCurrentColumn(root_settingpage, Qt.resolvedUrl("AppThemePage.qml"), {temp_app_theme: current_app_Theme})
          }

          ListItem {
              id: closeButtonItem
              height: closeButtonLayout.height
              divider.visible: false

              ListItemLayout {
                  id: closeButtonLayout
                  title.text: i18n.tr("Show close button on main page") + ": "
                  subtitle.text: show_Close_Button ? i18n.tr("Disable to hide close button") : i18n.tr("Enable to show close button")
                  Switch {
                      id: closeButtonSwitch
                      SlotsLayout.position: SlotsLayout.Last
                      checked: show_Close_Button
                      onCheckedChanged: {
                        show_Close_Button = checked;
                      }
                  }
              }

              onClicked: closeButtonSwitch.checked = !closeButtonSwitch.checked
          }

          ListItem {
              id: swipeDistanceItem
              height: swipeDistanceLayout.height
              divider.visible: false

              ListItemLayout {
                  id: swipeDistanceLayout
                  title.text: i18n.tr("Swipe distance")
                  subtitle.text: i18n.tr("Percent of screen width in portrait mode")

                  Label { text: swipe_Distance + " %" }
                  ProgressionSlot{}
              }
              onClicked: apl_main.addPageToCurrentColumn(root_settingpage, Qt.resolvedUrl("SwipeDistancePage.qml"), {temp_dist: swipe_Distance})
          }

          // ListItem {
          //     id: feedbackItem
          //     divider.visible: false
          //     height: feedbackLayout.height
          //     divider.visible: false
          //
          //     ListItemLayout {
          //         id: feedbackLayout
          //         title.text: i18n.tr("Please give feedback on SwipeToBack")
          //         ProgressionSlot{name: "external-link";color:top_text_color}
          //     }
          //     onClicked: Qt.openUrlExternally('https://cloud14.frostinfo.de/index.php/apps/polls/poll/4EYqNVND1FJ6spT7')
          // }
      }
    }
}
