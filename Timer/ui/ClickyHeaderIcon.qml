import QtQuick 2.4
import Ubuntu.Components 1.3

Button {
    id: root_clickyicon

    property alias iconname: icon.name
    property alias iconcolor: icon.color
    property alias iconsource: icon.source

    color: "transparent"
    width: units.gu(2)
    height: icon.height

    Icon {
        id: icon

        width: parent.width
        anchors.centerIn: parent
    }
}
