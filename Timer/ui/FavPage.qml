import QtQuick 2.4
import Ubuntu.Components 1.3
import "Storage.js" as Storage

Page {
    id: root_favpage

    property alias fav_listview: fav_listview
    property alias fav_listmodel: fav_listmodel

    signal selectAll()
    signal clearSelection()
    signal removeSelected()

    Component.onCompleted: {
        Storage.getFavs(fav_listmodel)
    }

    onSelectAll: {
        var tmp = [];
        for (var i=0; i<fav_listmodel.count; i++) {
            tmp.push(i)
        }
        fav_listview.ViewItems.selectedIndices = tmp
    }

    onClearSelection: {
        fav_listview.ViewItems.selectedIndices = []
    }

    onRemoveSelected: {
        var items = fav_listview.ViewItems.selectedIndices

        for (var i=0; i<items.length; i++ ){
            fav_listview.currentIndex = items[i]
            var currentstamp = fav_listmodel.get(fav_listview.currentIndex).favtimestamp
            Storage.deleteFavs(currentstamp)
        }
        fav_listmodel.clear()
        clearSelection()
        Storage.getFavs(fav_listmodel)
    }

    header: PageHeader {
        id: main_header

        SwipeToAction {
          apl_page: root_favpage;
          swipeDistance: swipe_Distance;
          isOneCol: true;
        }

        title: i18n.tr("Favourites")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color}
        trailingActionBar.actions: [
            Action {
                visible: !fav_listview.ViewItems.selectMode
                iconName: "add"
                text: i18n.tr("Add new")

                onTriggered: {
                    apl_main.addPageToCurrentColumn(root_favpage, edit_timer_page, {isEdit: false, isFromMain: false})
                    edit_timer_page.time_setter.h = 0
                    edit_timer_page.time_setter.m = 0
                    edit_timer_page.time_setter.s = 0
                    edit_timer_page.timer_name_field.text = ""
                    edit_timer_page.timer_name_field.forceActiveFocus()
                    alarmsound = defaultsound
                }
            },
            Action {
                visible: fav_listview.ViewItems.selectMode
                iconName: "delete"
                text: i18n.tr("Delete")
                enabled: fav_listview.ViewItems.selectedIndices.length !== 0
                onTriggered: {
                    removeSelected()
                    fav_listview.ViewItems.selectMode = false
                }
            },
            Action {
                visible: fav_listview.ViewItems.selectMode
                iconName: fav_listview.ViewItems.selectedIndices.length == fav_listview.count ? "select-none" : "select"
                text: i18n.tr("Select All")
                onTriggered: {
                    if (iconName == "select"){
                        selectAll()
                    }
                    else {
                        clearSelection()
                    }
                }
            }
        ]
        leadingActionBar.actions: [
            Action {
                id: leaveSelectmode
                visible: fav_listview.ViewItems.selectMode
                text: "cancel"
                iconSource: Qt.resolvedUrl(backIcon)
                onTriggered: {
                    clearSelection()
                    fav_listview.ViewItems.selectMode = false
                }
            },
            Action {
                visible: isWide
                text: "close"
                iconName: "close"
                onTriggered: {
                    isOneColumnLayout = true
                    apl_main.removePages(root_favpage)
                }
            },
            Action {
                visible: !isWide && !fav_listview.ViewItems.selectMode
                text: "close"
                iconSource: Qt.resolvedUrl(backIcon)
                onTriggered: {
                    isOneColumnLayout = true
                    apl_main.removePages(root_favpage)
                }
            }
        ]
    }

    ListView {
        id: fav_listview

        anchors {
            top: main_header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        clip: true

        delegate: TimerListItem {}

        model: ListModel {
            id: fav_listmodel
        }
    }

    Label {
        id: info_text
        visible: fav_listmodel.count == 0 ? true : false
        width: parent.width - units.gu(4)
        anchors {
            top: main_header.bottom
            left: parent.left
            leftMargin: units.gu(2)
            topMargin: units.gu(2)
        }

        text: i18n.tr("You can add timers as favourites by pressing the + icon above.")
        wrapMode: Text.WordWrap
    }
}
