import QtQuick 2.4
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0
import "Storage.js" as Storage

Rectangle {
    id: root_timesettermain

    property string timername: timer_name_field.displayText
    property var timersound: defaultsound
    property var tempendtime
    property bool isZero: time_text.text == "00:00:00"

    property var timerlengthdigits
    property alias time_setter: new_time_setter
    property real restHeight: time_text.implicitHeight + timer_end_time.implicitHeight + timer_name_field.implicitHeight + start_button.implicitHeight + edit_sound.implicitHeight + units.gu(5)
    property real circleWidth: (root_timesettermain.width > root_timesettermain.height) && !isHigh ? Math.min( root_timesettermain.width/2, root_timesettermain.height - units.gu(4), units.gu(60) )
                                                                                                   : Math.min( root_timesettermain.width - units.gu(4), root_timesettermain.height - restHeight, ( root_timesettermain.height / 2 - (time_text.height + timer_end_time.height + units.gu(1.5)) ) * 2,  units.gu(60))


    function startTimer()
    {
        var stamp = Date.now().toString()
        var timerduration = new_time_setter.h * 60 * 60 * 1000 + new_time_setter.m * 60 * 1000 + new_time_setter.s * 1000
        var endtime = tempEndTime(new_time_setter.h, new_time_setter.m, new_time_setter.s)
        tempendtime = endtime - (endtime % 1000) // round to 1s
        timerlengthdigits = timeLenghtToDate(timerduration)
        timername = timername == "" ? i18n.tr("Timer")
                                    : timername
        alarm.reset()
        alarm.date = new Date(tempendtime)
        alarm.message = timername + " (" + timerlengthdigits + ")"
        alarm.sound = getSoundFile(timersound)
        alarm.save()

        timers_model.append({"nameOfTimer": timername,
                                "endTime": tempendtime,
                                "soundOfTimer": timersound,
                                "timerStamp": stamp,
                                "durationOfTimer": timerduration,
                                "isItRunning": true
                            })
        Storage.saveTimers(timername, tempendtime, timerduration, timersound, 1, stamp)
        new_timer_ldr.source = ""
    }

    function resetTimer()
    {
        new_time_setter.h = 0
        new_time_setter.m = 0
        new_time_setter.s = 0
        new_time_setter.timelength = 0
    }


    function endTime()
    {
        var endTimeInMs = tempEndTime(new_time_setter.h, new_time_setter.m, new_time_setter.s)
        var endTimeV = Qt.formatTime(new Date(endTimeInMs), "hh:mm:ss")
        var today = Qt.formatDateTime(new Date(), "d MMM yyyy")
        var endDay = Qt.formatDateTime(new Date(endTimeInMs), "d MMM yyyy")

        if (today === endDay) {
            return endTimeV
        }
        else {
            return i18n.tr("tomorrow %1").arg(endTimeV)
        }
    }

    color: main_back_color

    LiveTimer {
        id: checkEndTime

        frequency: isZero ? LiveTimer.Disabled : LiveTimer.Second
        // TRANSLATORS: It's about the time at which a timer will finish,
        // It will read for example "Finish 12:45"
        onTrigger: timer_end_time.text = i18n.tr("Finish %1").arg(endTime())
    }

    // RESET TIMER TO ZERO
    ClickyIcon {
        id: reset_button

        visible: !isZero
        iconcolor: theme.palette.normal.negative
        iconname: "reset"
        anchors {
            right: time_text.left
            rightMargin: units.gu(1)
            verticalCenter: time_text.verticalCenter
        }

        onClicked: resetTimer()

    }


    // TIMER TEXT CLOCK

    Button {
        id: hour_btn

        color: "transparent"
        width: time_text.height
        height: width
        anchors {
            left: time_text.left
            verticalCenter: time_text.verticalCenter
        }

        Keys.onPressed: {
            var key = event.key
            if(keys01.indexOf(key) > -1) {
                if(time_setter.h === 0 || time_setter.h >= 2) {
                    time_setter.h = keys01.indexOf(key)
                }
                else {
                    time_setter.h = 10 * time_setter.h + keys01.indexOf(key)
                }
            }
            else if(keys29.indexOf(key) > -1) {
                time_setter.h = keys29.indexOf(key) + 2
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.h < 11) {
                    time_setter.h += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.h > 0) {
                    time_setter.h -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if (!isZero) {
                    startTimer()
                    main_page.header = main_header
                }
            }
        }
    }

    Button {
        id: min_btn

        color: "transparent"
        width: time_text.height
        height: width
        anchors.centerIn: time_text

        Keys.onPressed: {
            var key = event.key
            if(keys05.indexOf(key) > -1) {
                if(time_setter.m === 0 || time_setter.m >= 6) {
                    time_setter.m = keys05.indexOf(key)
                }
                else {
                    time_setter.m = 10 * time_setter.m + keys05.indexOf(key)
                }
            }
            else if(keys69.indexOf(key) > -1) {
                if(time_setter.m === 0 || time_setter.m >= 6) {
                    time_setter.m = keys69.indexOf(key) + 6
                }
                else {
                    time_setter.m = 10 * time_setter.m + (keys69.indexOf(key) + 6)
                }
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.m < 59) {
                    time_setter.m += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.m > 0) {
                    time_setter.m -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if (!isZero) {
                    startTimer()
                    main_page.header = main_header
                }
            }
        }
    }

    Button {
        id: sec_btn

        color: "transparent"
        width: time_text.height
        height: width
        anchors {
            right: time_text.right
            verticalCenter: time_text.verticalCenter
        }

        Keys.onPressed: {
            var key = event.key
            if(keys05.indexOf(key) > -1) {
                if(time_setter.s === 0 || time_setter.s >= 6) {
                    time_setter.s = keys05.indexOf(key)
                }
                else {
                    time_setter.s = 10 * time_setter.s + keys05.indexOf(key)
                }
            }
            else if(keys69.indexOf(key) > -1) {
                if(time_setter.s === 0 || time_setter.s >= 6) {
                    time_setter.s = keys69.indexOf(key) + 6
                }
                else {
                    time_setter.s = 10 * time_setter.s + (keys69.indexOf(key) + 6)
                }
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.s < 59) {
                    time_setter.s += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.s > 0) {
                    time_setter.s -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if (!isZero) {
                    startTimer()
                    main_page.header = main_header
                }
            }
        }
    }

    // ADD TO FAVS
    ClickyIcon {
        id: add_to_fav_button

        visible: !isZero
        iconcolor: top_text_color
        iconsource: Qt.resolvedUrl("add-to-favs.svg")
        anchors {
            left: time_text.right
            leftMargin: units.gu(1)
            verticalCenter: time_text.verticalCenter
        }
        onClicked: {
            // load saving page
            isOneColumnLayout = false
            apl_main.addPageToNextColumn(apl_main.primaryPage, edit_timer_page, { isEdit: false, isFromMain: true })
            edit_timer_page.time_setter.h = new_time_setter.h
            edit_timer_page.time_setter.m = new_time_setter.m
            edit_timer_page.time_setter.s = new_time_setter.s
            edit_timer_page.timer_name_field.forceActiveFocus()
            edit_timer_page.timer_name_field.text = timername
            alarmsound = timersound
        }
    }

    // Timer time
    Text {
        id: time_text

        color: theme.palette.normal.activity
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }
        text: unitsdisplay(new_time_setter.h) + ":" + unitsdisplay(new_time_setter.m) + ":" + unitsdisplay(new_time_setter.s)
        font.pixelSize: Math.min( parent.width / 6, units.gu(10) )
    }

    // TIMER END TIME
    Label {
        id: timer_end_time

        visible: !isZero
        width: parent.width - units.gu(4)
        anchors {
            top: time_text.bottom
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment:  Text.AlignHCenter
    }

    // TIMER NAME
    TextField {
        id: timer_name_field

        width: Math.min(parent.width - units.gu(4), units.gu(56))
        anchors {
            top: timer_end_time.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        text: i18n.tr("Timer")
        placeholderText: i18n.tr("Timer name")
        //        onAccepted: if(accept_action.visible) accept_action.trigger()
        //        onFocusChanged: if(focus){time_setter.isInteractive = false}
        inputMethodHints: Qt.ImhNoPredictiveText

    }

    // TIMER SOUND
    ListItem {
        id: edit_sound

        width: Math.min(parent.width, units.gu(60))
        anchors{
            top: timer_name_field.bottom
            horizontalCenter: parent.horizontalCenter
        }
        divider.visible: false
        SlotsLayout {
            mainSlot: Label {
                id: sound_name
                text: getFileName(timersound)
            }
            Icon {
                color: sound_name.color
                // replace source with name once the icon is available from upstream
                // name: timersound === "No sound" ? "disable_stock_music" : "stock_music"
                source: timersound === "No sound" ? "disable_stock_music.svg" : "music-app-symbolic.svg"
                SlotsLayout.position: SlotsLayout.Leading;
                width: units.gu(2)
            }
            ProgressionSlot {
                color: sound_name.color
            }
        }
        onClicked: {
            sound_ldr.source = Qt.resolvedUrl("SoundPickerPage.qml")
            sound_ldr.item.temp_sound = timersound
        }
    }

    // TIME SETTER
    TimeSetter {
        id: new_time_setter

        anchors {
            top: edit_sound.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
            verticalCenter: undefined
        }
        width: circleWidth
        Behavior on scale {NumberAnimation{duration: UbuntuAnimation.BriskDuration}}

    }

    // CENTER CIRCLE
    Rectangle {
        id: smallcentercircle

        color: current_clock_theme == "Imitatio" ? "transparent"
                                           : main_back_color
        width: 3 * new_time_setter.hour_hand.width / 2
        height: width
        radius: height / 2
        anchors.centerIn: new_time_setter
        Behavior on width {NumberAnimation{duration: UbuntuAnimation.BriskDuration}}

    }

    // START BUTTON
    Button {
        id: start_button

        visible: !isZero // show only when needed
        color: theme.palette.normal.activity
        width: Math.min(parent.width - units.gu(4), units.gu(22) )
        height: units.gu(6)
        anchors{
            bottom: parent.bottom
            bottomMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }

        Icon {
            id: start_button_icon

            color: theme.palette.normal.activityText
            name: "media-playback-start"
            width: parent.height/2
            //height: units.gu(8)
            anchors.centerIn: parent
        }

        onClicked: {
            startTimer()
            main_page.header = main_header
        }
    }

    Loader {
        id: sound_ldr

        anchors.fill: parent
    }


    states: [
        State {
            name: "landscape"
            when: root_timesettermain.width > root_timesettermain.height && !isHigh
            PropertyChanges {
                target: time_setter;
                anchors.horizontalCenterOffset: root_timesettermain.width / 4
                anchors.top: undefined
                anchors.verticalCenter: root_timesettermain.verticalCenter
            }
            PropertyChanges {
                target: start_button;
                anchors.horizontalCenterOffset: - root_timesettermain.width/4
            }
            PropertyChanges {
                target: timer_end_time
                width: root_timesettermain.width/2 - units.gu(4)
                anchors.horizontalCenterOffset: - root_timesettermain.width/4
            }

            PropertyChanges {
                target: timer_name_field
                width: root_timesettermain.width/2 - units.gu(4)
                anchors.horizontalCenterOffset: -root_timesettermain.width/4
                anchors.topMargin: .2
            }

            PropertyChanges {
                target: time_text
                anchors.horizontalCenterOffset: -root_timesettermain.width/4
                font.pixelSize: root_timesettermain.width/12
            }

            PropertyChanges {
                target: edit_sound
                width: root_timesettermain.width / 2
                anchors.horizontalCenterOffset: - root_timesettermain.width / 4
            }

        }
    ]
}
